import React from 'react';
import './css/AboutUs.css';
import Container from '../fragment/Container';
import b1 from '../assets/img/b8.jpg';

const About = () => {
  return (
    <Container>
      <div className="About">
        <div className="about-us-content">
          <h2 className="about-us-title" style={{ marginTop: '20px' }}>LUZHAY</h2>  
          <p className="about-us-description">
          LHUZHAY, is Bhutanese music platform to share the rich tapestry of cultural music from our beloved country; Bhutan.  We believe that music has the power to connect, inspire, and transform lives. Our mission is to provide you with exceptional musical experience that enriches your journey and elevates your connection to the world of melodies and rhythms.
          </p>
          <p className="about-us-description">
          Our mission is to provide a digital platform that showcases the diverse sounds, rhythms, and melodies that define our cultural identity. <br/>
          </p>
          <img src={b1} alt="img" className="about-us-image" style={{width:300,height:300}} />
        </div>
      </div>
    </Container>
  );
};

export default About;