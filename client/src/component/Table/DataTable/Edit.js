import React, { useEffect, useState } from "react";
// import {useHistory} from 'react-router-dom';
import { toast } from 'react-toastify';
import "./DataTable.css";

const Edit = ({songs}) => {

  // let history = useHistory();



//   const getSongs = async() => {
//       try {
//           const response = await fetch("http://localhost:5000/song")
//           const jsonData = await response.json() 
//           setSongs(jsonData)
//       } catch (err) {
//           console.log(err.message)
//       }
//   }

//   useEffect(()=>{
//       getSongs();
//   },[])

  
  

  const category_options = [
    {
      label: 'Select', 
      value: ''
    },
    {
    label: 'Rigsar', 
    value: 'rigsar'
  },
  {
    label: 'Boedra', 
    value: 'boedra'
  },
  {
    label: 'Zhungdra', 
    value: 'zhungdra'
  }
];
const [type, setType] = useState(songs.type);

  
  const [name, setName] = useState(songs.name);
  const [author, setAuthor] = useState(songs.author);
  const [image, setImage] = useState(songs.image);
  const [song, setSong] = useState(songs.song);
  console.log(name)


  const updateForm =async e =>{
    e.preventDefault();
    try{
        const formData= new FormData();
        formData.append('type', type);
        formData.append('img', image);
        formData.append('name', name);
        formData.append('author', author);
        formData.append('song', song);

        const response = await fetch(`http://localhost:5000/song/${songs.id}`, {
                    method: "PUT",
                    body: formData
                  }).then((resp)=>{
                    window.location.reload('/')
                  })
    
    }catch(err){
        console.log(err)
    }
  }

    return (
  <>
    
 
    <div class="modal fade" id="staticBackdrop" 
 
    data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true"
    >
      <div class="modal-dialog"
         id={`id${songs.id}`}>
      <div class="modal-content">
      <div className="wrapper">
          <form action="">
            <div className="title">
              Update 
              <button onClick={()=>{setName(songs.name);setAuthor(songs.author);setType(songs.type);
            setImage(songs.image);setSong(songs.song)}}>
                &times;

              </button>
            </div>

            <div className="form">
            <div className="inputfield">
                  <label>Name</label>
                  <input type="text" className="input" value={name} onChange={e=>setName(e.target.value)}/>
              </div>  
              <div className="inputfield">
                  <label>Author</label>
                  <input type="text" className="input" value={author} onChange={e=> setAuthor(e.target.value)}/>
              </div> 
              <div className="inputfield">
                <label>Category</label>
                <div className="custom_select">
                  <select value={type} onChange={e => setType(e.target.value)} id="type">
                  {category_options.map((category_options) => (
                      <option key={category_options.value} value={category_options.value}>{category_options.label}</option>
                      ))}
                  </select>
                </div>
            </div> 
              <div className="inputfield">
                  <label>Image</label>
                  <input type="file" name='image' 
                  className="input"  onChange={e=>setImage(e.target.files[0])} />
              </div>
              <div className="inputfield">
                  <label>Song</label>
                  <input type="file" name='song' className="input" onChange={e=>setSong(e.target.files[0])} />
              </div>   
             
              <div className="inputfield">
                <button type="button" className="btn btn-secondary mr-5" data-bs-dismiss="modal"
                 onClick={()=>{setName(songs.name);setAuthor(songs.author);setType(songs.type);
                    setImage(songs.image);setSong(songs.song)}}
                >Close</button>
                <button type="submit" className="btn uploadbutton" onClick={e=>updateForm(e)}>Update</button>
              </div>
            </div>
            </form> 
        </div>
        </div>      
      </div>
  </div> 
     
</>
    );
  }
  export default Edit;

  