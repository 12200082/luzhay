import React, { useEffect, useState } from "react";
// import {useHistory} from 'react-router-dom';
import { toast } from 'react-toastify';
import "./DataTable.css";
import Edit from "./Edit";

const DataDetails = () => {

  // let history = useHistory();

  const[songs, setSongs] = useState([]);

  const getSongs = async() => {
      try {
          const response = await fetch("http://localhost:5000/song")
          const jsonData = await response.json() 
          setSongs(jsonData)
      } catch (err) {
          console.log(err.message)
      }
  }

  useEffect(()=>{
      getSongs();
  },[])

  const handleDelete = async(id) => {
    try {
      const response = await fetch(`http://localhost:5000/song/${id}`, {
        method: 'DELETE'
      });
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      setSongs(songs.filter(song => song.id !== id));
      toast.success('Data Delete Successfully', {
        position: "top-center",
        autoClose: 3000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        theme: "light",
        });
    } catch(err) {
      console.error(err);
    }
  };
  

  const category_options = [
    {
      label: 'Select', 
      value: ''
    },
    {
    label: 'Rigsar', 
    value: 'rigsar'
  },
  {
    label: 'Boedra', 
    value: 'boedra'
  },
  {
    label: 'Zhungdra', 
    value: 'zhungdra'
  }
];
const [type, setType] = useState(category_options[0].value);

  
  const [name, setName] = useState("");
  const [author, setAuthor] = useState("");
  const [image, setImage] = useState(null);
  const [song, setSong] = useState(null);

  const handleSubmit = async (id) => {
    id.preventDefault();
    const formData = new FormData();
    formData.append('type', type);
    formData.append('img', image);
    formData.append('name', name);
    formData.append('author', author);
    formData.append('song', song);

    try{
      const response = await fetch(`http://localhost:5000/song/${id}`, {
        method: "PUT",
        body: formData
      })
      toast.success('song update Successfully')
      setTimeout(() => {
        window.location.reload();
      }, 2000);
      
      if (response.ok) {
        console.log('song update successfully!');
      } else {
        console.error('Failed to update song.');
      }

    }catch (err){
      console.log(err)
    }
  };

  const handleImage = (e) => {
    setImage(e.target.files[0])  
  };

  const handleSong = (e) => {
    setSong(e.target.files[0])  
  };
 
    return (
  <>
    
    <table className="table">
        <thead>
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Author</th>
                <th scope="col">Category</th>
                <th scope="col">Image</th>
                <th scope="col">Song</th>
                <th scope="col">Edit</th>
                <th scope="col">Delete</th>
            </tr>
        </thead>
        <tbody>
          {songs.map((songs) => {
            const{id, name, author, type, img, song} = songs;
            return (
              <tr key={song.id}>
              <td data-label='Name'>{name}</td>
              <td data-label='Author'>{author}</td>
              <td data-label='Category'>{type}</td>
              <td data-label='Image'><img src={`http://localhost:5000/image/${img}`} alt="Image" /></td>
              <td data-label='Song'>
                <audio controls>
                    <source src={`http://localhost:5000/image/${song}`} type="audio/mpeg" />
                </audio>
              </td>
              <td data-label='Edit'>
                  <button type="button" onClick={() => handleSubmit(id)} class="edit_btn" data-bs-toggle="modal" data-bs-target="#staticBackdrop">
                      Edit
                  </button>
                  <Edit songs={songs}/>
              </td>
              <td data-label='Delete'><button className="delete_btn" onClick={() => handleDelete(id)}>Delete</button></td>
            </tr>
            )
            
          })}
            
        </tbody>
    </table>
    {/* <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
      <div class="modal-dialog">
      <div class="modal-content">
      <div className="wrapper">
          <form action="">
            <div className="title">
              Update Music
            </div>
            <div className="form">
            <div className="inputfield">
                  <label>Name</label>
                  <input type="text" className="input" value={name} onChange={e=> setName(e.target.value)}/>
              </div>  
              <div className="inputfield">
                  <label>Author</label>
                  <input type="text" className="input" value={author} onChange={e=> setAuthor(e.target.value)}/>
              </div> 
              <div className="inputfield">
                <label>Category</label>
                <div className="custom_select">
                  <select value={type} onChange={e => setType(e.target.value)} id="type">
                  {category_options.map((category_options) => (
                      <option key={category_options.value} value={category_options.value}>{category_options.label}</option>
                      ))}
                  </select>
                </div>
            </div> 
              <div className="inputfield">
                  <label>Image</label>
                  <input type="file" name='image' className="input" onChange={handleImage} />
              </div>
              <div className="inputfield">
                  <label>Song</label>
                  <input type="file" name='song' className="input" onChange={handleSong} />
              </div>   
             
              <div className="inputfield">
                <button type="button" className="btn btn-secondary mr-5" data-bs-dismiss="modal">Close</button>
                <button type="submit" className="btn uploadbutton">Upload</button>
              </div>
            </div>
            </form> 
        </div>
        </div>      
      </div>
  </div> */}
    
</>
    );
  }
  export default DataDetails;

  