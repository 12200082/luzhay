import  React, { useState, useEffect} from 'react';
import { toast } from 'react-toastify';
import "./data_form.css";
// import { useHistory } from "react-router-dom";

function DataForm () {

  // let history = useHistory();

  const category_options = [
    {
      label: 'Select', 
      value: ''
    },
    {
    label: 'Rigsar', 
    value: 'rigsar'
  },
  {
    label: 'Boedra', 
    value: 'boedra'
  },
  {
    label: 'Zhungdra', 
    value: 'zhungdra'
  }
];
const [type, setType] = useState(category_options[0].value);

  
  const [name, setName] = useState("");
  const [author, setAuthor] = useState("");
  const [image, setImage] = useState(null);
  const [song, setSong] = useState(null);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const formData = new FormData();
    formData.append('type', type);
    formData.append('img', image);
    formData.append('name', name);
    formData.append('author', author);
    formData.append('song', song);

    try{
      const response = await fetch('http://localhost:5000/song', {
        method: "Post",
        body: formData
      })
      toast.success('song added Successfully')
      setTimeout(() => {
        window.location.reload();
      }, 2000);
      
      if (response.ok) {
        console.log('song uploaded successfully!');
      } else {
        console.error('Failed to upload song.');
      }

    }catch (err){
      console.log(err)
    }
  };

  const handleImage = (e) => {
    setImage(e.target.files[0])  
  };

  const handleSong = (e) => {
    setSong(e.target.files[0])  
  };

    return (
      <>
        <div className="wrapper">
          <form action="">
            <div className="title">
              Upload Music
            </div>
            <div className="form">
            <div className="inputfield">
                  <label>Name</label>
                  <input type="text" className="input" value={name} onChange={e=> setName(e.target.value)}/>
              </div>  
              <div className="inputfield">
                  <label>Author</label>
                  <input type="text" className="input" value={author} onChange={e=> setAuthor(e.target.value)}/>
              </div> 
              <div className="inputfield">
                <label>Category</label>
                <div className="custom_select">
                  <select value={type} onChange={e => setType(e.target.value)} id="type">
                  {category_options.map((category_options) => (
                      <option key={category_options.value} value={category_options.value}>{category_options.label}</option>
                      ))}
                  </select>
                </div>
            </div> 
              <div className="inputfield">
                  <label>Image</label>
                  <input type="file" name='image' className="input" onChange={handleImage} />
              </div>
              <div className="inputfield">
                  <label>Song</label>
                  <input type="file" name='song' className="input" onChange={handleSong} />
              </div>   
             
              <div className="inputfield">
                <button 
                type="submit"
                className="btn uploadbutton" 
                onClick={handleSubmit}>Upload</button>
              </div>
            </div>
            </form>
        </div>
</>
    );
  }
  export default DataForm;