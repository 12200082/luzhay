import React, {useEffect, useState} from "react";
import Home from "../components/Pages/Home";
import {BrowserRouter as Router, Route, Switch, Redirect} from 'react-router-dom';
import Login from "../components/Pages/Login";
import {ThemeContext, themes} from "../api/Theme";
import SignUp from "../components/Pages/SignUp";
import SignIn from "../components/Pages/SignIn";
import HomePage from "../pages/Home/Index";
import SignIn2 from "../pages/SignIn2";

const App = () => {
    
    const checkAuthenticated = async () => {
        try {
          const res = await fetch("http://localhost:5000/song/verify", {
            method: "POST",
            headers: { jwt_token: localStorage.token }
          });
    
          const parseRes = await res.json();
    
          parseRes === true ? setIsAuthenticated(true) : setIsAuthenticated(false);
        } catch (err) {
          console.error(err.message);
        }
      };
    
      useEffect(() => {
        checkAuthenticated();
      }, []);
    
      const [isAuthenticated, setIsAuthenticated] = useState(false);
    
      const setAuth = boolean => {
        setIsAuthenticated(boolean);
      };
      //admin
      const checkAuthenticated2 = async () => {
        try {
          const res = await fetch("http://localhost:5000/admin/verify", {
            method: "POST",
            headers: { jwt_token: localStorage.token }
          });
    
          const parseRes = await res.json();
    
          parseRes === true ? setIsAuthenticated2(true) : setIsAuthenticated2(false);
        } catch (err) {
          console.error(err.message);
        }
      };
    
      useEffect(() => {
        checkAuthenticated2();
      }, []);
    
      const [isAuthenticated2, setIsAuthenticated2] = useState(false);
    
      const setAuth2 = boolean => {
        setIsAuthenticated2(boolean);
      };

    return (
        <ThemeContext.Provider value={themes.light}>
            <>
                <Router>
                    <Switch>
                        <Route path="/" exact component={Login}/>

                        <Route
                            path="/signin"
                            render={props =>
                                !isAuthenticated ? (
                                <SignIn {...props} setAuth={setAuth}/>
                                ) : (
                                <Redirect to="/home" />
                                )
                            }
                        />
                        <Route path="/signup" component={SignUp} />
                        <Route
                        path="/home"
                        render={props =>
                                isAuthenticated ? (
                                <Home {...props} setAuth={setAuth} />
                                ) : (
                              <Redirect to="/signin" />
                              )
                            }
                        />
                        {/* admin */}
                        <Route
                            path="/signin2"
                            render={props =>
                                !isAuthenticated2 ? (
                                <SignIn2 {...props} setAuth={setAuth2}/>
                                ) : (
                                <Redirect to="/admin" />
                                )
                            }
                        />
                        <Route
                        path="/admin"
                        render={props =>
                                isAuthenticated2 ? (
                                <HomePage {...props} setAuth={setAuth2} />
                                ) : (
                              <Redirect to="/signin2" />
                              )
                            }
                        />

                        {/* <Route path="/admin" component={HomePage}/> */}
                        
                    </Switch>
                </Router>
            </>
        </ThemeContext.Provider>
    );
}

export default App;