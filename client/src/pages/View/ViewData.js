import React from 'react';
import "./ViewData.css";
import DataDetails from '../../component/Table/DataTable/data';

const ViewData = () => {
  return (
    <>
    <section className="data container-fluid px-md-5 mt-5" id="data">
    <div className="content">
            <h3>UPLOADED MUSIC</h3>
        </div>
       <DataDetails/>
    </section>
    </>
  )
}

export default ViewData;
