import React, {useState, useEffect} from "react";
import FormData from "../Form/Form";
import {toast} from 'react-toastify';
import './Index.css'
import { Link, useHistory } from "react-router-dom";

import ViewData from "../View/ViewData";
import Footer from "../../component/Footer/Footer";
import Users from "../Users/users";


import logo  from '../../components/assets/img/l2.png'
export default function HomePage ({setAuth}) {


    let history = useHistory();


  // const logout = (e) => {
  //   e.preventDefault();
  //   localStorage.removeItem("token");
  //   setAuth2(false);
  //   history.push('/signin2');
  //   toast.success('Sign-Out Successfully', {
  //     position: "top-center",
  //     autoClose: 3000,
  //     hideProgressBar: false,
  //     closeOnClick: true,
  //     pauseOnHover: true,
  //     draggable: true,
  //     progress: undefined,
  //     theme: "light",
  //     });
      
  // }
  const logout = async e => {
    e.preventDefault();
    try {
      localStorage.removeItem("token");
      setAuth(false);
      history.push('/');
      toast.success("Logout successfully");
    } catch (err) {
      console.error(err.message);
    }
  };

    return (
        <div style={{height:'100%',background:'#A2D5F2 '}}>
       <div style={{width:'100%'}}>
        <nav style={{display:'flex',alignItems:'center',justifyContent:'space-between',flexWrap:'wrap',paddingInline:'80px',background:'#fff'}}>
            <div>
                <img src={logo} alt="logo" width={50} height={50} />
            </div>
            <ul id="navbar" style={{display:'flex',flexDirection:'row',alignItems:'center',flexWrap:'wrap',gap:'40px',listStyle:'none'}}>
                <li><a style={{textDecoration:'none',fontWeight:'500'}} href="#heroBanner">Home</a></li>
                <li><a  style={{textDecoration:'none',fontWeight:'500'}} href="#users">Users</a></li>
                <li><a style={{textDecoration:'none',fontWeight:'500'}} href="#form_id">Upload</a></li>
                <li><a  style={{textDecoration:'none',fontWeight:'500'}} href="#data">View</a></li>
                <li><a onClick={e => logout(e)}  style={{textDecoration:'none',fontWeight:'500'}}>Sign-Out</a></li>
            </ul>
        </nav>
    </div>
    <section id="heroBanner">
      <h1 style={{fontWeight:'900',color:'#fff',fontSize:'60px'}}>Welcome to Luzhay Admin</h1>
    </section>
    <Users/>
    <FormData/>
    <ViewData/>
    <Footer />
</div>
    )
};